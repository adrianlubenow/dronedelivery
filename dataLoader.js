const fs = require('fs').promises;

async function loadData(filePath) {
    try {
        const rawData = await fs.readFile(filePath, 'utf8');
        return JSON.parse(rawData);
    } catch (error) {
        console.error('Error loading data:', error);
        return null; 
    }
}

module.exports = { loadData };
