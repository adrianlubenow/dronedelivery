const { loadData } = require('./dataLoader');
const { simulateDeliveries } = require('./simulationManager');
const SimulationManager = require('./simulationManager');
const Warehouse = require('./warehouse');
const Customer = require('./customer');
const Order = require('./order');
const Drone = require('./drone');

async function main() {
    const data = await loadData('./input.json');

    const poweredOn = data.output.poweredOn;
    const programMinutes = data.output.minutes.program;
    const realLifeMilliseconds = data.output.minutes.real;
    const verbosity = poweredOn;

    const timeScale = realLifeMilliseconds / (programMinutes * 60 * 1000);

    if (!poweredOn) {
        console.log("Simulation is configured to run without detailed output.");
    }

    const simulationManager = new SimulationManager(data);

    simulationManager.processOrdersWithTimeScaling(timeScale, verbosity);
}

main().catch(console.error);