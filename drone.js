const { calculateDistance, findClosestWarehouse } = require('./utils');

class Drone {
    constructor(speed = 1, batteryCapacityW, powerConsumptionWPerMinute, warehouses) {
        this.speed = speed; 
        this.batteryCapacityW = batteryCapacityW;
        this.powerConsumptionWPerMinute = powerConsumptionWPerMinute;
        this.currentBatteryW = batteryCapacityW; 
        this.currentLocation = { x: 0, y: 0 };
        this.totalDeliveryTime = 0;
        this.warehouses = warehouses;
    }

    calculateTravelTime(destination) {
        const distance = calculateDistance(this.currentLocation, destination);
        const time = distance / this.speed;
        return time;
    }

    canCompleteTrip(destination) {
        const travelTimeToDestination = this.calculateTravelTime(destination); 
        const batteryUsageForTrip = this.calculateBatteryUsage(travelTimeToDestination); 
        return this.currentBatteryW >= batteryUsageForTrip;
    }

    moveTo(newLocation) {

        this.simulateOperationalDelay();
        this.checkForIntermediaryStop(newLocation);

        const travelTimeToCustomer = this.calculateTravelTime(newLocation);
        const batteryUsedToCustomerW = this.powerConsumptionWPerMinute * travelTimeToCustomer;

        if (batteryUsedToCustomerW > this.currentBatteryW) {
            console.log("Drone battery depleted. Returning to warehouse for recharge.");
            this.rechargeAtNearestWarehouse();
            return false;
        } 

        this.currentBatteryW -= batteryUsedToCustomerW;

        console.log("Drone starting move to destination at coordinates: (",  newLocation.x, ") (" ,newLocation.y, ")");
        this.currentLocation = newLocation;

        this.totalDeliveryTime += travelTimeToCustomer;

        this.waitAtCustomer();

        return true;
    }

    calculateBatteryUsage(travelTimeMinutes) {
        return this.powerConsumptionWPerMinute * travelTimeMinutes;
    }

    recharge() {
        const rechargeTime = 20;
        this.currentBatteryW = this.batteryCapacityW;
        this.totalDeliveryTime += rechargeTime;
        console.log(`Drone recharged to ${this.currentBatteryW}W at warehouse.`);

    }

    rechargeAtNearestWarehouse() {
        const nearestWarehouse = findClosestWarehouse(this.currentLocation, this.warehouses);
        const travelTimeToWarehouse = this.calculateTravelTime(nearestWarehouse);

        this.totalDeliveryTime += travelTimeToWarehouse;
        this.currentBatteryW -= this.powerConsumptionWPerMinute * travelTimeToWarehouse;

        console.log(`Drone starting move to destination at coordinates (${nearestWarehouse.x}, ${nearestWarehouse.y}).`);
        this.currentLocation = nearestWarehouse;

        this.recharge();
    }

    checkForIntermediaryStop(destination) {
        if (this.currentBatteryW < this.batteryCapacityW * 0.2) {
            console.log("Intermediary stop needed for recharge.");
            this.rechargeAtNearestWarehouse();
        }

        if (Math.random() < 0.1) { 
            console.log("Intermediary stop needed due to weather.");
            this.waitOutBadWeather();
        }
    }

    simulateOperationalDelay() {
        const hasDelay = Math.random() < 0.3;
        if (hasDelay) {
            const delayTime = Math.floor(Math.random() * 6);
            this.totalDeliveryTime += delayTime;
            console.log(`Operational delay encountered: ${delayTime} minutes.`);
        }
    }

    waitOutBadWeather() {
        const waitTime = Math.floor(Math.random() * (15 - 5 + 1)) + 5;
        this.totalDeliveryTime += waitTime;
        console.log(`Waiting out bad weather for ${waitTime} minutes.`);
    }

    waitAtCustomer(){
        const waitTime = Math.floor(Math.random() * 11);
        this.totalDeliveryTime += waitTime;
        console.log(`Waited at customer for ${waitTime} minutes.`);
    }
}

module.exports = Drone;