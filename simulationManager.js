const Warehouse = require('./warehouse');
const Customer = require('./customer');
const Order = require('./order');
const Drone = require('./drone');
const { calculateDistance, findClosestWarehouse } = require('./utils');
const { loadData } = require('./dataLoader');

class SimulationManager {

    constructor(data) {
        this.warehouses = data.warehouses.map(warehouse => new Warehouse(warehouse.x, warehouse.y, warehouse.name));
        this.customers = data.customers.map(customer => new Customer(customer.id, customer.name, customer.coordinates));
        this.orders = data.orders;
        this.data = data;
        this.typesOfDrones = data.typesOfDrones.sort((a, b) => this.parseCapacity(a.capacity) - this.parseCapacity(b.capacity));
        this.drones = [];
        this.operationalDayLength = 480;
        this.cumulativeDeliveryTime = 0;
        this.deliveryTimes = [];
    }

    async reloadOrders() {
        const newData = await loadData('./input.json');
        return newData.orders;
    }

    processNewOrders() {
        this.reloadOrders().then((newOrdersData) => {
            const existingOrderSignatures = new Set(this.orders.map(order => this.serializeOrder(order)));
            
            const newOrders = newOrdersData.filter(order => !existingOrderSignatures.has(this.serializeOrder(order)));
    
            if (newOrders.length > 0) {
                console.log("Found", newOrders.length, "new orders.");
                this.orders.push(...newOrders);
            }
        });
    }

    serializeOrder(order) {
        const productListString = JSON.stringify(order.productList);
        return `customer${order.customerId}:products${productListString}`;
    }

    estimateAndPrepareDrones() {
        let temporaryDrones = [];
        this.orders.forEach(order => {
            const customer = this.customers.find(c => c.id === order.customerId);
            const closestWarehouse = findClosestWarehouse(customer.coordinates, this.warehouses);
            let droneAssigned = false;
    
            for (const drone of temporaryDrones) {
                const toCustomer = drone.calculateTravelTime(customer.coordinates);
                const toWarehouse = drone.calculateTravelTime(closestWarehouse);
                const roundTripTime = toCustomer + toWarehouse;
    
                if (drone.totalDeliveryTime + roundTripTime <= this.operationalDayLength) {
                    drone.totalDeliveryTime += roundTripTime;
                    droneAssigned = true;
                    break;
                }
            }
    
            if (!droneAssigned) {
                const newDrone = this.findOrAddDroneForOrder(customer.coordinates, closestWarehouse)
                const toCustomer = newDrone.calculateTravelTime(customer.coordinates);
                const toWarehouse = newDrone.calculateTravelTime(closestWarehouse);
                newDrone.totalDeliveryTime = toCustomer + toWarehouse;
                temporaryDrones.push(newDrone);
            }
        });
    
        this.drones = temporaryDrones;
    }

    attemptToDeliverOrder(order, customer, closestWarehouse) {
        let drone = this.findOrAddDroneForOrder(customer.coordinates, closestWarehouse);
        if (!drone || drone.totalDeliveryTime > this.operationalDayLength) {
            return false;
        }

        let deliveryStartTime = drone.totalDeliveryTime;

        const initialCumulativeTime = this.cumulativeDeliveryTime;

        if (!drone.moveTo(customer.coordinates)) {
            console.error(`Failed to deliver order to customer ID: ${customer.id}. Drone unable to reach destination.`);
            return false;
        }

        let deliveryEndTime = drone.totalDeliveryTime;
        let simulatedDeliveryTime = deliveryEndTime - deliveryStartTime;
        this.deliveryTimes.push(simulatedDeliveryTime);

        const timeSpentOnDelivery = drone.totalDeliveryTime - initialCumulativeTime;
        this.cumulativeDeliveryTime += timeSpentOnDelivery;

        if (this.cumulativeDeliveryTime > this.operationalDayLength) {
            console.error("Exceeded operational day length. Cancelling order...");
            return false;
        }

        return true;
    }

    findOrAddDroneForOrder(destination, closestWarehouse) {
        let suitableDrone = null;
        let minAdditionalTime = Infinity;

        const averageOperationalDelay = 5; //average delay due to operational issues.
        const averageWaitingTimeAtCustomer = 3; //average delay due to waiting at customer.
    
        this.drones.forEach(drone => {
            const travelTime = drone.calculateTravelTime(destination);
            const estimatedTotalTime = travelTime + averageOperationalDelay + averageWaitingTimeAtCustomer;

            if (drone.totalDeliveryTime + estimatedTotalTime <= this.operationalDayLength && drone.canCompleteTrip(destination)) {
                if (estimatedTotalTime < minAdditionalTime) {
                    suitableDrone = drone;
                    minAdditionalTime = estimatedTotalTime;
                }
            }
        });
    
        if (!suitableDrone) {
            suitableDrone = this.addNewDrone(destination, closestWarehouse);
        }
    
        return suitableDrone;
    }

    addNewDrone(destination, closestWarehouse) {
        const distanceToDestination = calculateDistance(closestWarehouse, destination);
        const roundTripDistance = distanceToDestination * 2;
    
        for (let droneType of this.typesOfDrones) {
            const batteryCapacityW = this.parseCapacity(droneType.capacity);
            const powerConsumptionWPerMinute = this.parseCapacity(droneType.consumption);
    
            const speed = 1;
            const travelTimeToDestination = roundTripDistance / speed;
            const batteryUsageForTrip = travelTimeToDestination * powerConsumptionWPerMinute;
    
            if (batteryCapacityW >= batteryUsageForTrip) {
                return this.initializeDrone(droneType, closestWarehouse);
            }
        }
    
        console.error("Failed to find a suitable drone type that can complete the trip.");
        return null;
    }

    initializeDrone(droneType, startLocation) {
        const batteryCapacityKW = this.parseCapacity(droneType.capacity);
        const powerConsumptionWPerMinute = this.parseCapacity(droneType.consumption);

        const newDrone = new Drone(1, batteryCapacityKW, powerConsumptionWPerMinute, this.warehouses);
        newDrone.currentLocation = startLocation;

        newDrone.totalDeliveryTime = 0;

        return newDrone;
    }

    parseCapacity(capacity) {
        if (capacity.endsWith('kW')) {
            return parseFloat(capacity) * 1000; // Convert kW to W
        } else if (capacity.endsWith('W')) {
            return parseFloat(capacity);
        }
        return 0;
    }

    calculateAverageDeliveryTime() {
        if (this.deliveryTimes.length === 0) {
            return 0;
        }
        const totalDeliveryTime = this.deliveryTimes.reduce((acc, time) => acc + time, 0);
        return totalDeliveryTime / this.deliveryTimes.length;
    }

    async processOrdersWithTimeScaling(timeScale, verbosity) {
        const startTime = Date.now();
        const milliseconds = 60 * 1000 * timeScale;
        const totalOperationalDayLengthInRealMilliseconds = this.operationalDayLength * milliseconds;
    
        this.estimateAndPrepareDrones();
        
        console.log("Starting orders count: ", this.orders.length);

        const checkInterval = setInterval(() => this.processNewOrders(), milliseconds);

        for (let order of this.orders) {
            const currentTime = Date.now();
            const elapsedRealTime = currentTime - startTime;
    
            if (elapsedRealTime > totalOperationalDayLengthInRealMilliseconds) {
                if (verbosity) {
                    console.log("Operational day ended in simulation time.");
                }
                break;
            }
    
            const customer = this.customers.find(c => c.id === order.customerId);
            if (!customer) {
                console.error(`Customer with ID ${order.customerId} not found.`);
                continue;
            }
    
            const closestWarehouse = findClosestWarehouse(customer.coordinates, this.warehouses);
            const success = this.attemptToDeliverOrder(order, customer, closestWarehouse);
    
            if (verbosity) {
                console.log(`Order for customer ID: ${order.customerId} ${success ? 'delivered' : 'could not be delivered'}.`);
            }
    
            const remainingOrders = this.orders.length - this.orders.indexOf(order) - 1;
            if (remainingOrders > 0) {
                const waitTimePerOrder = (totalOperationalDayLengthInRealMilliseconds - elapsedRealTime) / remainingOrders;
                await new Promise(resolve => setTimeout(resolve, waitTimePerOrder));
            }
        }

        clearInterval(checkInterval);

        if(verbosity)
        {
            console.log("Total orders for the day: ", this.orders.length);
            console.log("All orders processed. Drone fleet size at end of day:", this.drones.length);
            const maxDeliveryTime = this.drones.reduce((max, drone) => Math.max(max, drone.totalDeliveryTime), 0);
            console.log("Total operational day length based on parallel drone operation:", maxDeliveryTime.toFixed(2), "minutes.");
            console.log("Delivery time report for each drone:");
            this.drones.forEach((drone, index) => {
                console.log("Drone ", index + 1, ": ", drone.totalDeliveryTime.toFixed(2), "minutes");
            });

            const averageDeliveryTime = this.calculateAverageDeliveryTime();
            console.log("Average delivery time per order: ", averageDeliveryTime.toFixed(2), "minutes.");
        }
    
        if (!verbosity) {
            console.log("Simulation completed. Summary:");
        }
    }
    
}



module.exports = SimulationManager;
