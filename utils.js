function calculateDistance(pointA, pointB) {
    return Math.sqrt(Math.pow(pointA.x - pointB.x, 2) + Math.pow(pointA.y - pointB.y, 2));
}

function findClosestWarehouse(location, warehouses) {
    return warehouses.reduce((closest, warehouse) => {
        const distanceToCurrent = calculateDistance(location, { x: warehouse.x, y: warehouse.y });
        const distanceToClosest = closest ? calculateDistance(location, { x: closest.x, y: closest.y }) : Infinity;
        return distanceToCurrent < distanceToClosest ? warehouse : closest;
    });
}

module.exports = {
    calculateDistance,
    findClosestWarehouse,
};
